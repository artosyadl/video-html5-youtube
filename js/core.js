'use strict';
// fixed svg show
//-----------------------------------------------------------------------------
svg4everybody();

// checking if element for page
//-----------------------------------------------------------------------------------
function isOnPage(selector) {
    return ($(selector).length) ? $(selector) : false;
}

// # TODO: dropdown
// # -----------------------------------------------------------------------------------
$('.el-dropdown .ui.dropdown').dropdown();




// # TODO: search
// # -----------------------------------------------------------------------------------
var options = {
    url: "/json/countries.json",
    getValue: "name",
    list: {
        match: {
            enabled: true
        }
    },
    requestDelay: 10
};

$("#autocomplete").easyAutocomplete(options);


// # TODO: header
// # -----------------------------------------------------------------------------------
$(window).on("scroll", function () {
    if ($(this).scrollTop() > 20) {
        $('.header').addClass('mod-fixed');
    } else {
        $('.header').removeClass('mod-fixed');
    }
});


// # TODO: feature slider
// # -----------------------------------------------------------------------------------
if (isOnPage($('.js-featured-video-slider'))) {
    $('.js-featured-video-slider').slick({
        centerMode: false,
        centerPadding: '15px',
        slidesToShow: 4,
        speed: 300,
        arrows: true,
        dots: false,
        infinite: true,
        nextArrow: '<button type="button" class="slick-next"><svg class="icon icon-r-arrow"><use xlink:href="img/sprite.svg#r-arrow"></use></svg></button>',
        prevArrow: '<button type="button" class="slick-prev"><svg class="icon icon-l-arrow"><use xlink:href="img/sprite.svg#l-arrow"></use></svg></button>',
        responsive: [
            {
                breakpoint: 992,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '40px',
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 768,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '40px',
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 520,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '20px',
                    slidesToShow: 1
                }
            }
        ]
    });
}

// custom jQuery validation
//-----------------------------------------------------------------------------------
var validator = {
    init: function() {
        $('form').each(function() {
            var name = $(this).attr('name');
            if (valitatorRules.hasOwnProperty(name)) {
                var rules = valitatorRules[name];
                $(this).validate({
                    rules: rules,
                    errorElement: 'b',
                    errorClass: 'error',
                    focusInvalid: false,
                    focusCleanup: false,
                    onfocusout: function(element) {
                        var $el = validator.defineElement($(element));
                        $el.valid();
                    },
                    errorPlacement: function(error, element) {
                        validator.setError($(element), error);
                    },
                    highlight: function(element, errorClass, validClass) {
                        var $el = validator.defineElement($(element)),
                            $elWrap = $el.closest('.el-field');
                        if ($el) {
                            $el.removeClass(validClass).addClass(errorClass);
                            $elWrap.removeClass('show-check');
                            if ($el.closest('.ui.dropdown').length) {
                                $el.closest('.ui.dropdown').addClass('error');
                            }
                        }
                    },
                    unhighlight: function(element, errorClass, validClass) {
                        var $el = validator.defineElement($(element)),
                            $elWrap = $el.closest('.el-field');
                        if ($el) {
                            $el.removeClass(errorClass).addClass(validClass);
                            if ($elWrap.hasClass('check-valid')) {
                                $elWrap.addClass('show-check');
                            }
                            $el.closest('el-field').addClass('show-check');
                            if ($el.val() == '') {
                                $el.removeClass('valid');
                            }
                            if ($el.closest('.ui.dropdown').length) {
                                $el.closest('.ui.dropdown').removeClass('error');
                            }
                        }
                    },
                    messages: {
                        'user_email': {
                            required: 'Поле обязательное',
                            email: 'Неправильный формат E-mail'
                        },
                        'user_name': {
                            required: 'Поле обязательное',
                            letters: 'Неправильный формат имени',
                            minlength: 'Не меньше 2 символов'
                        },
                        'user_login': {
                            required: 'Поле обязательное',
                            email: 'Неправильный формат E-mail'
                        },
                        'user_password': {
                            required: 'Поле обязательное',
                            minlength: 'Не менее 6-ти символов'
                        },
                        'user_password_confirm': {
                            required: 'Вы не подтвердили пароль',
                            minlength: 'Не менее 6-ти символов',
                            equalTo: 'Пароли должны совпадать'
                        },
                        'user_phone': {
                            required: 'Поле обязательное',
                            digits: 'Неправильный формат номера'
                        }
                    }
                });
            }
        });
    },
    setError: function($el, message) {
        $el = this.defineElement($el);
        if ($el) this.domWorker.error($el, message);
    },
    defineElement: function($el) {
        return $el;
    },
    domWorker: {
        error: function($el, message) {
            if ($el.attr('type') == 'file') $el.parent().addClass('file-error');
            $el.addClass('error');
            $el.after(message);
        }
    }
};


// rule for form namespace
//-----------------------------------------------------------------------------------
var valitatorRules = {
    'form_one': {
        'user_login': {
            required: true,
            email: true
        },
        'user_name': {
            required: true,
            minlength: 2
        },
        'user_email': {
            required: true,
            email: true
        },
        'user_phone': {
            required: true,
            digits: true
        },
        'user_password': {
            required: true,
            minlength: 6
        },
        'user_password_confirm': {
            required: true,
            minlength: 6,
            equalTo: "#user_password"
        }
    }

};

// custom rules
//-----------------------------------------------------------------------------------
$.validator.addMethod("email", function(value) {
    if (value == '') return true;
    var regexp = /[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
    return regexp.test(value);
});

$.validator.addMethod("letters", function(value, element) {
    return this.optional(element) || /^[^1-9!@#\$%\^&\*\(\)\[\]:;,.?=+_<>`~\\\/"]+$/i.test(value);
});

$.validator.addMethod("digits", function(value, element) {
    return this.optional(element) || /^(\+?\d+)?\s*(\(\d+\))?[\s-]*([\d-]*)$/i.test(value);
});

$.validator.addMethod("valueNotEquals", function(value, element) {
    if (value == "") return false
    else return true
});

//  validator init
//-----------------------------------------------------------------------------------
validator.init();

// search page
function pageWidget(pages) {
    var widgetWrap = $('<div class="widget_wrap"><ul class="widget_list"></ul></div>');
    widgetWrap.prependTo("body");
    for (var i = 0; i < pages.length; i++) {
        if (pages[i][0] === '#') {
            $('<li class="widget_item"><a class="widget_link" href="' + pages[i] +'">' + pages[i] + '</a></li>').appendTo('.widget_list');
        } else {
            $('<li class="widget_item"><a class="widget_link" href="' + pages[i] + '.html' + '">' + pages[i] + '</a></li>').appendTo('.widget_list');
        }
    }
    var widgetStilization = $('<style>body {position:relative} .widget_wrap{position:fixed;top:0;left:0;z-index:9999;padding:20px 20px;background:#222;border-bottom-right-radius:10px;-webkit-transition:all .3s ease;transition:all .3s ease;-webkit-transform:translate(-100%,0);-ms-transform:translate(-100%,0);transform:translate(-100%,0)}.widget_wrap:after{content:" ";position:absolute;top:0;left:100%;width:24px;height:24px;background:#222 url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQAgMAAABinRfyAAAABGdBTUEAALGPC/xhBQAAAAxQTFRF////////AAAA////BQBkwgAAAAN0Uk5TxMMAjAd+zwAAACNJREFUCNdjqP///y/DfyBg+LVq1Xoo8W8/CkFYAmwA0Kg/AFcANT5fe7l4AAAAAElFTkSuQmCC) no-repeat 50% 50%;cursor:pointer}.widget_wrap:hover{-webkit-transform:translate(0,0);-ms-transform:translate(0,0);transform:translate(0,0)}.widget_item{padding:0 0 10px}.widget_link{color:#fff;text-decoration:none;font-size:15px;}.widget_link:hover{text-decoration:underline} </style>');
    widgetStilization.prependTo(".widget_wrap");
}

$(document).ready(function($) {
    pageWidget(['index', 'video', 'catalogue']);
});
"use strict";

$.fn.html5video = function () {

    var $wrapper = $(this),
        $video = $("video", this),
        $controls = $(".video-controls", $wrapper),
        $playPause = $(".play-pause", $wrapper),
        $playCenter = $(".center-btm-play", $wrapper),
        $btnMute = $(".btn-mute", $wrapper),
        $current = $(".video-current", $wrapper),
        $scrubber = $(".video-scrubber", $wrapper),
        $progress = $(".video-progress", $wrapper),
        $duration = $(".video-duration", $wrapper),
        $fullScreenBtn = $('.full-screen', $wrapper),
        $volume_wrapper = $(".volume", $wrapper),
        $volume_bar = $(".volume-bar", $wrapper),
        $position = true,
        $percentage = true;

    
    $video.removeAttr("controls");
    $video.on("loadedmetadata", function () {
        updateVolume(0, 0.52);
        $duration.text(formatTime(this.duration));
        $controls.show();
    });


    // # TODO: Play/Pause
    // # -----------------------------------------------------------------------------------
    $playPause.on("click", function () {

        var $this = $(this),
            video = $video[0];

        if (video.paused) {
            video.play();
            $this.addClass("pause");
            $wrapper.addClass("play");
        } else {
            video.pause();
            $this.removeClass("pause");
            $wrapper.removeClass("play");

        }
    });

    $playCenter.on("click", function () {


        if ($video[0].paused) {
            $video[0].play();
            $playPause.addClass("pause");
            $wrapper.addClass("play");
        }
    });

    $btnMute.on("click", function () {

        var $this = $(this),
            video = $video[0];

        if (video.muted) {
            video.muted = false;
            $this.removeClass("muted");
            $volume_bar.css("width", $video[0].volume * 100 + "%");

        } else {
            video.muted = true;
            $this.addClass("muted");
            $volume_bar.css("width", 0);

        }
    });

    // # TODO: Update Time
    // # -----------------------------------------------------------------------------------
    
    $video.on("timeupdate", function (event) {
        onTrackedVideoFrame(this.currentTime, this.duration);
    });

    function onTrackedVideoFrame(currentTime, duration) {

        var percent = currentTime / duration;
        $current.text(formatTime(currentTime));
        $duration.text(formatTime(duration));
        $progress.width((percent * 100) + "%");
        if (percent == 1) {
            $playPause.removeClass("pause");
        }
    }

    // # TODO: Scrubber/Progress Bar
    // # -----------------------------------------------------------------------------------
    var timeDrag = false;

    $scrubber.on("mousedown", function (e) {
        timeDrag = true;
        updateScrubber(e.pageX);
    });

    $(document).on("mouseup", function (e) {
        if (timeDrag) {
            timeDrag = false;
            updateScrubber(e.pageX);
        }
    });

    $(document).on("mousemove", function (e) {
        if (timeDrag) {
            updateScrubber(e.pageX);
        }
    });

    function updateScrubber(x) {

        var maxduration = $video[0].duration,
            position = x - $scrubber.offset().left,
            percent = 100 * position / $scrubber.width();

        if (percent > 100) {
            percent = 100;
        }
        if (percent < 0) {
            percent = 0;
        }
        $progress.width((percent) + "%");
        $video[0].currentTime = maxduration * percent / 100;
    };


    // # TODO: Helper Functions
    // # -----------------------------------------------------------------------------------
    
    function formatTime(seconds) {

        var minutes = Math.floor(seconds / 60),
            seconds = Math.floor(seconds % 60);

        seconds = (seconds >= 10) ? seconds : "0" + seconds;
        minutes = (minutes >= 10) ? minutes : minutes;

        return minutes + ":" + seconds;
    }

    $video.bind("ended", function () {
        // do cool shizz here
    });


    // # TODO: Update Volume Functions
    // # -----------------------------------------------------------------------------------
    function updateVolume(x, vol) {
        if (vol) {
            $percentage = vol * 100;
        } else {
            $position = x - $volume_wrapper.offset().left;
            $percentage = 100 * $position / $volume_wrapper.width();
        }

        if ($percentage > 100) {
            $percentage = 100;
        }
        if ($percentage < 0) {
            $percentage = 0;
        }
        $volume_bar.css("width", $percentage + "%");
        $video[0].volume = $percentage / 100;

        if ($video[0].volume == 0) {
            $btnMute.addClass("muted");
        } else if ($video[0].volume > 0.5) {
            $btnMute.removeClass("muted");
        } else {
            $btnMute.removeClass("muted");
        }
    }


    // # TODO: Volume Drag
    // # -----------------------------------------------------------------------------------
    var volumeDrag = false;
    $volume_wrapper.on("mousedown", function (e) {
        volumeDrag = true;
        $video[0].muted = false;
        $btnMute.removeClass("muted");
        updateVolume(e.pageX);
    });

    $(document).on("mouseup", function (e) {
        if (volumeDrag) {
            volumeDrag = false;
            updateVolume(e.pageX);
        }
    });

    $(document).on("mousemove", function (e) {
        if (volumeDrag) {
            updateVolume(e.pageX);
        }
    });


    // # TODO: Full Screen Button
    // # -----------------------------------------------------------------------------------
    $fullScreenBtn.on('click', function (e) {
        e.preventDefault();
        launchFullscreen();

    });

    function launchFullscreen() {
        if ($video[0].requestFullscreen) {
            $video[0].requestFullscreen();
        } else if ($video[0].mozRequestFullScreen) {
            $video[0].mozRequestFullScreen();
        } else if ($video[0].webkitRequestFullscreen) {
            $video[0].webkitRequestFullscreen();
        } else if ($video[0].msRequestFullscreen) {
            $video[0].msRequestFullscreen();
        }
    }

}

// # TODO: Add controls to DOM
// # -----------------------------------------------------------------------------------

$(".video-player").each(function () {
    var controlsHTML = '<button class="center-btm-play"></button><div class="video-controls" style="display:none"><div class="video-scrubber"><div class="video-progress"></div></div><button class="play-pause"></button><button class="btn-mute"></button><div class="volume" title="Set volume"><span class="volume-bar"></span></div><div class="video-time"><div class="video-current">0:00</div>/<div class="video-duration">0:00</div></div><button class="full-screen"></button></div>';
    $(this).append(controlsHTML);
    $(this).html5video();
});





"use strict";

$.fn.youtubeVideo = function (player) {

    var $wrapper = $(this),
        $videoWrap = $(".video-youtube-wrap", this),
        $videoId = $videoWrap.attr('data-id-video'),
        $play = $(".btn-play", $wrapper),
        $pause = $(".btn-pause", $wrapper),
        $playCenter = $(".center-btm-play-youtube", $wrapper),
        $btnMute = $(".btn-mute", $wrapper),
        $current = $(".video-current", $wrapper),
        $duration = $(".video-duration", $wrapper),
        $scrubber = $(".video-scrubber", $wrapper),
        $progressVideo = $(".video-progress", $wrapper),
        $fullScreenBtn = $('.full-screen-youtube', $wrapper),
        $volume_wrapper = $(".volume", $wrapper),
        $volume_bar = $(".volume-bar", $wrapper),
        time_update_interval = 0,
        $position = true,
        $percentage = true;


    $videoWrap.css('background-image', 'url(https://img.youtube.com/vi/' + $videoId + '/maxresdefault.jpg)');
    // $videoItem.css('background-image', 'url(https://img.youtube.com/vi/' + videoId + '/maxresdefault.jpg)');
    // $videoItem.css('background-image', 'url(https://img.youtube.com/vi/'+videoId+'/sddefault.jpg)');


    initialize()
    function initialize() {

        updateTimerDisplay();
        updateVolume(0, 0.52);
        clearInterval(time_update_interval);

        time_update_interval = setInterval(function () {
            updateTimerDisplay();
        }, 1000);

    }

// # TODO: Scrubber/Progress Bar
    // # -----------------------------------------------------------------------------------
    var timeDrag = false;

    $scrubber.on("mousedown", function (e) {
        timeDrag = true;
        updateScrubber(e.pageX);
    });

    $(document).on("mouseup", function (e) {
        if (timeDrag) {
            timeDrag = false;
            updateScrubber(e.pageX);
        }
    });

    $(document).on("mousemove", function (e) {
        if (timeDrag) {
            updateScrubber(e.pageX);
        }
    });

    function updateScrubber(x) {

        var maxduration = player.getDuration(),
            position = x - $scrubber.offset().left,
            percent = 100 * position / $scrubber.width();

        if (percent > 100) {
            percent = 100;
        }
        if (percent < 0) {
            percent = 0;
        }
        $progressVideo.width((percent) + "%");

        player.seekTo(maxduration * percent / 100);
    };



// # TODO: Sound volume
// # -----------------------------------------------------------------------------------
    $btnMute.on('click', function () {
        var mute_toggle = $(this);

        if (player.isMuted()) {
            player.unMute();
            mute_toggle.removeClass('muted');
            $volume_bar.css("width", player.getVolume() + "%");
        }
        else {
            player.mute();
            mute_toggle.addClass('muted');
            $volume_bar.css("width", 0);
        }
    });


    var volumeDrag = false;
    $volume_wrapper.on("mousedown", function (e) {
        volumeDrag = true;
        updateVolume(e.pageX);
    });

    $(document).on("mouseup", function (e) {
        if (volumeDrag) {
            volumeDrag = false;
            updateVolume(e.pageX);
        }
    });

    $(document).on("mousemove", function (e) {
        if (volumeDrag) {
            updateVolume(e.pageX);
        }
    });

    function updateVolume(x, vol) {

        if (vol) {
            $percentage = vol * 100;
        } else {
            $position = x - $volume_wrapper.offset().left;
            $percentage = 100 * $position / $volume_wrapper.width();
        }

        if ($percentage > 100) {
            $percentage = 100;
        }
        if ($percentage < 0) {
            $percentage = 0;
        }

        $volume_bar.css("width", $percentage + "%");
        player.setVolume($percentage / 1);


        if ($percentage / 1 == 0) {
            $btnMute.addClass("muted");
        } else {
            $btnMute.removeClass("muted");
        }

    }

// # TODO: Play/Pause
// # -----------------------------------------------------------------------------------
    $play.on('click', function () {
        player.playVideo();
        $wrapper.addClass('start play');
    });

    $playCenter.on('click', function () {
        player.playVideo();
        $wrapper.addClass('start play');
    });

    $pause.on('click', function () {
        player.pauseVideo();
        $wrapper.removeClass('play');
    });


// # TODO: Update current time text display.
// # -----------------------------------------------------------------------------------
    function updateTimerDisplay() {
        $current.text(formatTime(player.getCurrentTime()));
        $duration.text(formatTime(player.getDuration()));

        $progressVideo.width((player.getCurrentTime() / player.getDuration() * 100) + "%");

        if (player.getCurrentTime() == player.getDuration()) {
            $wrapper.removeClass("play");
        }
    }

// # TODO: Helper Functions
// # -----------------------------------------------------------------------------------
    function formatTime(time) {
        time = Math.round(time);

        var minutes = Math.floor(time / 60),
            seconds = time - minutes * 60;

        seconds = seconds < 10 ? '0' + seconds : seconds;

        return minutes + ":" + seconds;
    }

// # TODO: Full Screen Button
// # -----------------------------------------------------------------------------------
    $fullScreenBtn.on('click', function () {
        playFullscreen();
    });

    if (document.addEventListener) {
        document.addEventListener('webkitfullscreenchange', exitHandler, false);
        document.addEventListener('mozfullscreenchange', exitHandler, false);
        document.addEventListener('fullscreenchange', exitHandler, false);
        document.addEventListener('MSFullscreenChange', exitHandler, false);
    }
    function exitHandler() {
        if (document.webkitIsFullScreen || document.mozFullScreen || document.msFullscreenElement !== null) {
            if ((document.webkitIsFullScreen == false) || (document.mozFullScreen == false) || (document.msFullscreenElement == false)){
                $('body').removeClass('full');
            }
        }
    }
    function playFullscreen() {

        var docElm = $wrapper[0];
        var isInFullScreen = (document.fullscreenElement && document.fullscreenElement !== null) ||
            (document.webkitFullscreenElement && document.webkitFullscreenElement !== null) ||
            (document.mozFullScreenElement && document.mozFullScreenElement !== null) ||
            (document.msFullscreenElement && document.msFullscreenElement !== null);

        if (!isInFullScreen) {
            $('body').addClass('full');
            if (docElm.requestFullscreen) {
                docElm.requestFullscreen();
            } else if (docElm.mozRequestFullScreen) {
                docElm.mozRequestFullScreen();
            } else if (docElm.webkitRequestFullScreen) {
                docElm.webkitRequestFullScreen();
            } else if (docElm.msRequestFullscreen) {
                docElm.msRequestFullscreen();
            }
        } else {
            $('body').removeClass('full');
            if (document.exitFullscreen) {
                document.exitFullscreen();
            } else if (document.webkitExitFullscreen) {
                document.webkitExitFullscreen();
            } else if (document.mozCancelFullScreen) {
                document.mozCancelFullScreen();
            } else if (document.msExitFullscreen) {
                document.msExitFullscreen();
            }
        }

    }

}



var tag = document.createElement('script');
tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

function onYouTubeIframeAPIReady() {
    $(".video-player-youtube").each(function () {
        var playerid = $(this).find('.video-placeholder').attr('id'),
            videoid = $(this).find('.video-youtube-wrap').attr('data-id-video');

        var curplayer = createPlayer(playerid, videoid);
    });
}

function createPlayer(playerID, videoID) {
    return new YT.Player(playerID, {
        width: 600,
        height: 400,
        videoId: videoID,
        playerVars: {
            'modestbranding': 1,
            'rel': 0,
            'controls': 0,
            'showinfo': 0,
            'autoplay': 0,
            'playsinline': 1,
            'loop': 1,
            'color': 'red',
            'autohide': 0,
            'iv_load_policy': 3,
            'nologo': 1,
            'fs': 0
        },
        events: {
            onReady: onPlayerReady
        }
    })
    
}

function onPlayerReady(event) {
    var player = event.target,
        $wraper = $('#'+player.a.id).closest('.video-player-youtube'),
        controlsHtml = '<button class="center-btm-play-youtube"></button><div class="video-controls"><div class="video-scrubber"><div class="video-progress"></div></div><button id="play" class="icon-play btn-play"></button><button id="pause" class="icon-pause btn-pause"></button><button id="mute-toggle" class="icon-mute btn-mute"></button><div class="volume" title="Set volume"><span class="volume-bar"></span></div><div class="video-time"><span id="current-time" class="video-current">0:00</span> / <span id="duration" class="video-duration">0:00</span></div><button class="full-screen-youtube"></button></div>';

    $wraper.append(controlsHtml);
    $wraper.youtubeVideo(player);
}


















