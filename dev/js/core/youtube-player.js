"use strict";

$.fn.youtubeVideo = function (player) {

    var $wrapper = $(this),
        $videoWrap = $(".video-youtube-wrap", this),
        $videoId = $videoWrap.attr('data-id-video'),
        $play = $(".btn-play", $wrapper),
        $pause = $(".btn-pause", $wrapper),
        $playCenter = $(".center-btm-play-youtube", $wrapper),
        $btnMute = $(".btn-mute", $wrapper),
        $current = $(".video-current", $wrapper),
        $duration = $(".video-duration", $wrapper),
        $scrubber = $(".video-scrubber", $wrapper),
        $progressVideo = $(".video-progress", $wrapper),
        $fullScreenBtn = $('.full-screen-youtube', $wrapper),
        $volume_wrapper = $(".volume", $wrapper),
        $volume_bar = $(".volume-bar", $wrapper),
        time_update_interval = 0,
        $position = true,
        $percentage = true;


    $videoWrap.css('background-image', 'url(https://img.youtube.com/vi/' + $videoId + '/maxresdefault.jpg)');
    // $videoItem.css('background-image', 'url(https://img.youtube.com/vi/' + videoId + '/maxresdefault.jpg)');
    // $videoItem.css('background-image', 'url(https://img.youtube.com/vi/'+videoId+'/sddefault.jpg)');


    initialize()
    function initialize() {

        updateTimerDisplay();
        updateVolume(0, 0.52);
        clearInterval(time_update_interval);

        time_update_interval = setInterval(function () {
            updateTimerDisplay();
        }, 1000);

    }

// # TODO: Scrubber/Progress Bar
    // # -----------------------------------------------------------------------------------
    var timeDrag = false;

    $scrubber.on("mousedown", function (e) {
        timeDrag = true;
        updateScrubber(e.pageX);
    });

    $(document).on("mouseup", function (e) {
        if (timeDrag) {
            timeDrag = false;
            updateScrubber(e.pageX);
        }
    });

    $(document).on("mousemove", function (e) {
        if (timeDrag) {
            updateScrubber(e.pageX);
        }
    });

    function updateScrubber(x) {

        var maxduration = player.getDuration(),
            position = x - $scrubber.offset().left,
            percent = 100 * position / $scrubber.width();

        if (percent > 100) {
            percent = 100;
        }
        if (percent < 0) {
            percent = 0;
        }
        $progressVideo.width((percent) + "%");

        player.seekTo(maxduration * percent / 100);
    };



// # TODO: Sound volume
// # -----------------------------------------------------------------------------------
    $btnMute.on('click', function () {
        var mute_toggle = $(this);

        if (player.isMuted()) {
            player.unMute();
            mute_toggle.removeClass('muted');
            $volume_bar.css("width", player.getVolume() + "%");
        }
        else {
            player.mute();
            mute_toggle.addClass('muted');
            $volume_bar.css("width", 0);
        }
    });


    var volumeDrag = false;
    $volume_wrapper.on("mousedown", function (e) {
        volumeDrag = true;
        updateVolume(e.pageX);
    });

    $(document).on("mouseup", function (e) {
        if (volumeDrag) {
            volumeDrag = false;
            updateVolume(e.pageX);
        }
    });

    $(document).on("mousemove", function (e) {
        if (volumeDrag) {
            updateVolume(e.pageX);
        }
    });

    function updateVolume(x, vol) {

        if (vol) {
            $percentage = vol * 100;
        } else {
            $position = x - $volume_wrapper.offset().left;
            $percentage = 100 * $position / $volume_wrapper.width();
        }

        if ($percentage > 100) {
            $percentage = 100;
        }
        if ($percentage < 0) {
            $percentage = 0;
        }

        $volume_bar.css("width", $percentage + "%");
        player.setVolume($percentage / 1);


        if ($percentage / 1 == 0) {
            $btnMute.addClass("muted");
        } else {
            $btnMute.removeClass("muted");
        }

    }

// # TODO: Play/Pause
// # -----------------------------------------------------------------------------------
    $play.on('click', function () {
        player.playVideo();
        $wrapper.addClass('start play');
    });

    $playCenter.on('click', function () {
        player.playVideo();
        $wrapper.addClass('start play');
    });

    $pause.on('click', function () {
        player.pauseVideo();
        $wrapper.removeClass('play');
    });


// # TODO: Update current time text display.
// # -----------------------------------------------------------------------------------
    function updateTimerDisplay() {
        $current.text(formatTime(player.getCurrentTime()));
        $duration.text(formatTime(player.getDuration()));

        $progressVideo.width((player.getCurrentTime() / player.getDuration() * 100) + "%");

        if (player.getCurrentTime() == player.getDuration()) {
            $wrapper.removeClass("play");
        }
    }

// # TODO: Helper Functions
// # -----------------------------------------------------------------------------------
    function formatTime(time) {
        time = Math.round(time);

        var minutes = Math.floor(time / 60),
            seconds = time - minutes * 60;

        seconds = seconds < 10 ? '0' + seconds : seconds;

        return minutes + ":" + seconds;
    }

// # TODO: Full Screen Button
// # -----------------------------------------------------------------------------------
    $fullScreenBtn.on('click', function () {
        playFullscreen();
    });

    if (document.addEventListener) {
        document.addEventListener('webkitfullscreenchange', exitHandler, false);
        document.addEventListener('mozfullscreenchange', exitHandler, false);
        document.addEventListener('fullscreenchange', exitHandler, false);
        document.addEventListener('MSFullscreenChange', exitHandler, false);
    }
    function exitHandler() {
        if (document.webkitIsFullScreen || document.mozFullScreen || document.msFullscreenElement !== null) {
            if ((document.webkitIsFullScreen == false) || (document.mozFullScreen == false) || (document.msFullscreenElement == false)){
                $('body').removeClass('full');
            }
        }
    }
    function playFullscreen() {

        var docElm = $wrapper[0];
        var isInFullScreen = (document.fullscreenElement && document.fullscreenElement !== null) ||
            (document.webkitFullscreenElement && document.webkitFullscreenElement !== null) ||
            (document.mozFullScreenElement && document.mozFullScreenElement !== null) ||
            (document.msFullscreenElement && document.msFullscreenElement !== null);

        if (!isInFullScreen) {
            $('body').addClass('full');
            if (docElm.requestFullscreen) {
                docElm.requestFullscreen();
            } else if (docElm.mozRequestFullScreen) {
                docElm.mozRequestFullScreen();
            } else if (docElm.webkitRequestFullScreen) {
                docElm.webkitRequestFullScreen();
            } else if (docElm.msRequestFullscreen) {
                docElm.msRequestFullscreen();
            }
        } else {
            $('body').removeClass('full');
            if (document.exitFullscreen) {
                document.exitFullscreen();
            } else if (document.webkitExitFullscreen) {
                document.webkitExitFullscreen();
            } else if (document.mozCancelFullScreen) {
                document.mozCancelFullScreen();
            } else if (document.msExitFullscreen) {
                document.msExitFullscreen();
            }
        }

    }

}



var tag = document.createElement('script');
tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

function onYouTubeIframeAPIReady() {
    $(".video-player-youtube").each(function () {
        var playerid = $(this).find('.video-placeholder').attr('id'),
            videoid = $(this).find('.video-youtube-wrap').attr('data-id-video');

        var curplayer = createPlayer(playerid, videoid);
    });
}

function createPlayer(playerID, videoID) {
    return new YT.Player(playerID, {
        width: 600,
        height: 400,
        videoId: videoID,
        playerVars: {
            'modestbranding': 1,
            'rel': 0,
            'controls': 0,
            'showinfo': 0,
            'autoplay': 0,
            'playsinline': 1,
            'loop': 1,
            'color': 'red',
            'autohide': 0,
            'iv_load_policy': 3,
            'nologo': 1,
            'fs': 0
        },
        events: {
            onReady: onPlayerReady
        }
    })
    
}

function onPlayerReady(event) {
    var player = event.target,
        $wraper = $('#'+player.a.id).closest('.video-player-youtube'),
        controlsHtml = '<button class="center-btm-play-youtube"></button><div class="video-controls"><div class="video-scrubber"><div class="video-progress"></div></div><button id="play" class="icon-play btn-play"></button><button id="pause" class="icon-pause btn-pause"></button><button id="mute-toggle" class="icon-mute btn-mute"></button><div class="volume" title="Set volume"><span class="volume-bar"></span></div><div class="video-time"><span id="current-time" class="video-current">0:00</span> / <span id="duration" class="video-duration">0:00</span></div><button class="full-screen-youtube"></button></div>';

    $wraper.append(controlsHtml);
    $wraper.youtubeVideo(player);
}


















