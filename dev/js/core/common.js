'use strict';
// fixed svg show
//-----------------------------------------------------------------------------
svg4everybody();

// checking if element for page
//-----------------------------------------------------------------------------------
function isOnPage(selector) {
    return ($(selector).length) ? $(selector) : false;
}

// # TODO: dropdown
// # -----------------------------------------------------------------------------------
$('.el-dropdown .ui.dropdown').dropdown();




// # TODO: search
// # -----------------------------------------------------------------------------------
var options = {
    url: "/json/countries.json",
    getValue: "name",
    list: {
        match: {
            enabled: true
        }
    },
    requestDelay: 10
};

$("#autocomplete").easyAutocomplete(options);


// # TODO: header
// # -----------------------------------------------------------------------------------
$(window).on("scroll", function () {
    if ($(this).scrollTop() > 20) {
        $('.header').addClass('mod-fixed');
    } else {
        $('.header').removeClass('mod-fixed');
    }
});


// # TODO: feature slider
// # -----------------------------------------------------------------------------------
if (isOnPage($('.js-featured-video-slider'))) {
    $('.js-featured-video-slider').slick({
        centerMode: false,
        centerPadding: '15px',
        slidesToShow: 4,
        speed: 300,
        arrows: true,
        dots: false,
        infinite: true,
        nextArrow: '<button type="button" class="slick-next"><svg class="icon icon-r-arrow"><use xlink:href="img/sprite.svg#r-arrow"></use></svg></button>',
        prevArrow: '<button type="button" class="slick-prev"><svg class="icon icon-l-arrow"><use xlink:href="img/sprite.svg#l-arrow"></use></svg></button>',
        responsive: [
            {
                breakpoint: 992,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '40px',
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 768,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '40px',
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 520,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '20px',
                    slidesToShow: 1
                }
            }
        ]
    });
}
