"use strict";

$.fn.html5video = function () {

    var $wrapper = $(this),
        $video = $("video", this),
        $controls = $(".video-controls", $wrapper),
        $playPause = $(".play-pause", $wrapper),
        $playCenter = $(".center-btm-play", $wrapper),
        $btnMute = $(".btn-mute", $wrapper),
        $current = $(".video-current", $wrapper),
        $scrubber = $(".video-scrubber", $wrapper),
        $progress = $(".video-progress", $wrapper),
        $duration = $(".video-duration", $wrapper),
        $fullScreenBtn = $('.full-screen', $wrapper),
        $volume_wrapper = $(".volume", $wrapper),
        $volume_bar = $(".volume-bar", $wrapper),
        $position = true,
        $percentage = true;

    
    $video.removeAttr("controls");
    $video.on("loadedmetadata", function () {
        updateVolume(0, 0.52);
        $duration.text(formatTime(this.duration));
        $controls.show();
    });


    // # TODO: Play/Pause
    // # -----------------------------------------------------------------------------------
    $playPause.on("click", function () {

        var $this = $(this),
            video = $video[0];

        if (video.paused) {
            video.play();
            $this.addClass("pause");
            $wrapper.addClass("play");
        } else {
            video.pause();
            $this.removeClass("pause");
            $wrapper.removeClass("play");

        }
    });

    $playCenter.on("click", function () {


        if ($video[0].paused) {
            $video[0].play();
            $playPause.addClass("pause");
            $wrapper.addClass("play");
        }
    });

    $btnMute.on("click", function () {

        var $this = $(this),
            video = $video[0];

        if (video.muted) {
            video.muted = false;
            $this.removeClass("muted");
            $volume_bar.css("width", $video[0].volume * 100 + "%");

        } else {
            video.muted = true;
            $this.addClass("muted");
            $volume_bar.css("width", 0);

        }
    });

    // # TODO: Update Time
    // # -----------------------------------------------------------------------------------
    
    $video.on("timeupdate", function (event) {
        onTrackedVideoFrame(this.currentTime, this.duration);
    });

    function onTrackedVideoFrame(currentTime, duration) {

        var percent = currentTime / duration;
        $current.text(formatTime(currentTime));
        $duration.text(formatTime(duration));
        $progress.width((percent * 100) + "%");
        if (percent == 1) {
            $playPause.removeClass("pause");
        }
    }

    // # TODO: Scrubber/Progress Bar
    // # -----------------------------------------------------------------------------------
    var timeDrag = false;

    $scrubber.on("mousedown", function (e) {
        timeDrag = true;
        updateScrubber(e.pageX);
    });

    $(document).on("mouseup", function (e) {
        if (timeDrag) {
            timeDrag = false;
            updateScrubber(e.pageX);
        }
    });

    $(document).on("mousemove", function (e) {
        if (timeDrag) {
            updateScrubber(e.pageX);
        }
    });

    function updateScrubber(x) {

        var maxduration = $video[0].duration,
            position = x - $scrubber.offset().left,
            percent = 100 * position / $scrubber.width();

        if (percent > 100) {
            percent = 100;
        }
        if (percent < 0) {
            percent = 0;
        }
        $progress.width((percent) + "%");
        $video[0].currentTime = maxduration * percent / 100;
    };


    // # TODO: Helper Functions
    // # -----------------------------------------------------------------------------------
    
    function formatTime(seconds) {

        var minutes = Math.floor(seconds / 60),
            seconds = Math.floor(seconds % 60);

        seconds = (seconds >= 10) ? seconds : "0" + seconds;
        minutes = (minutes >= 10) ? minutes : minutes;

        return minutes + ":" + seconds;
    }

    $video.bind("ended", function () {
        // do cool shizz here
    });


    // # TODO: Update Volume Functions
    // # -----------------------------------------------------------------------------------
    function updateVolume(x, vol) {
        if (vol) {
            $percentage = vol * 100;
        } else {
            $position = x - $volume_wrapper.offset().left;
            $percentage = 100 * $position / $volume_wrapper.width();
        }

        if ($percentage > 100) {
            $percentage = 100;
        }
        if ($percentage < 0) {
            $percentage = 0;
        }
        $volume_bar.css("width", $percentage + "%");
        $video[0].volume = $percentage / 100;

        if ($video[0].volume == 0) {
            $btnMute.addClass("muted");
        } else if ($video[0].volume > 0.5) {
            $btnMute.removeClass("muted");
        } else {
            $btnMute.removeClass("muted");
        }
    }


    // # TODO: Volume Drag
    // # -----------------------------------------------------------------------------------
    var volumeDrag = false;
    $volume_wrapper.on("mousedown", function (e) {
        volumeDrag = true;
        $video[0].muted = false;
        $btnMute.removeClass("muted");
        updateVolume(e.pageX);
    });

    $(document).on("mouseup", function (e) {
        if (volumeDrag) {
            volumeDrag = false;
            updateVolume(e.pageX);
        }
    });

    $(document).on("mousemove", function (e) {
        if (volumeDrag) {
            updateVolume(e.pageX);
        }
    });


    // # TODO: Full Screen Button
    // # -----------------------------------------------------------------------------------
    $fullScreenBtn.on('click', function (e) {
        e.preventDefault();
        launchFullscreen();

    });

    function launchFullscreen() {
        if ($video[0].requestFullscreen) {
            $video[0].requestFullscreen();
        } else if ($video[0].mozRequestFullScreen) {
            $video[0].mozRequestFullScreen();
        } else if ($video[0].webkitRequestFullscreen) {
            $video[0].webkitRequestFullscreen();
        } else if ($video[0].msRequestFullscreen) {
            $video[0].msRequestFullscreen();
        }
    }

}

// # TODO: Add controls to DOM
// # -----------------------------------------------------------------------------------

$(".video-player").each(function () {
    var controlsHTML = '<button class="center-btm-play"></button><div class="video-controls" style="display:none"><div class="video-scrubber"><div class="video-progress"></div></div><button class="play-pause"></button><button class="btn-mute"></button><div class="volume" title="Set volume"><span class="volume-bar"></span></div><div class="video-time"><div class="video-current">0:00</div>/<div class="video-duration">0:00</div></div><button class="full-screen"></button></div>';
    $(this).append(controlsHTML);
    $(this).html5video();
});




